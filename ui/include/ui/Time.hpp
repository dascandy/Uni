#pragma once

#include <Var.hpp>
#include <cstdint>

extern Val<int64_t> currentTimeMs; 

void updateTime();

