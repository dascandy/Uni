#pragma once

#include <string>
#include <map>
#include <ui/Texture.hpp>
#include <ui/SubTexture.hpp>
#include <ui/Attributes.hpp>
#include <ui/vertex.hpp>
#include <stile/Font.hpp>

struct FontFace;

class FontCache {
public:
  FontCache();
  ~FontCache();
  static FontCache& Instance();
  std::shared_ptr<Stile::Font> loadFont(const std::string& name);
private:
  std::map<std::string, std::shared_ptr<Stile::Font>> fonts;
};

struct FontFace {
public:
  enum class Unit {
    Pixels,
    Points,
    Centimeter,
    Millimeter,
  };
  FontFace() = default;
  FontFace(std::shared_ptr<Stile::Font> font, Unit unit, size_t size) 
  : font(font)
  , unit(unit)
  , size(size)
  {}
  void Render(std::vector<vertex>& vertices, const std::string& text, float left, float top, float width, float z, Align align) const;
  float size_px() const;
  int operator<=>(const FontFace&) const = default;
private:
  std::shared_ptr<Stile::Font> font;
  Unit unit = Unit::Pixels;
  size_t size = 0;
};


