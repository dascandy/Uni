#pragma once

#include <Var.hpp>
#include <vector>
#include <ui/vertex.hpp>
#include <map>
#include <SDL2/SDL.h>

class ShaderProgram;
struct vertex;
class Texture;

struct Width;
struct Height;
struct Stretch;

class Widget {
public:
  Widget();
  virtual ~Widget();
  virtual void Render(float parentX, float parentY, float parentZ, std::map<Texture*, std::vector<vertex>>&) = 0;
  virtual void Relayout() {}
  void set(Width width);
  void set(Height height);
  void set(Stretch);
  virtual bool HandleEvent(const SDL_Event&) { return false; }
  virtual bool stretch() { return stretch_; }
  Val<float> w, h, x, y;
  bool stretch_ = false;
};

#include "ui/Attributes.hpp"

