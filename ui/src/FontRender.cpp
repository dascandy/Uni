#include <ui/FontRender.hpp>
#include <ui/Attributes.hpp>
#include <vector>
#include <filesystem>
#include <fstream>
#include <span>
#include <ui/vertex.hpp>

FontCache& FontCache::Instance() {
  static FontCache cache;
  return cache;
}

FontCache::FontCache() {
}

FontCache::~FontCache() {
  fonts.clear();
}

std::shared_ptr<Stile::Font> FontCache::loadFont(const std::string& name) {
  if (auto it = fonts.find(name); it != fonts.end()) return it->second;
  return fonts[name] = Stile::LoadFont(name);
}

float FontFace::size_px() const {
  const float dpmm = 96 / 25.4;
  switch(unit) {
    default:
    case Unit::Pixels:
      return size;
    case Unit::Points:
      return size * 1.3333;
    case Unit::Centimeter:
      return size * 10 * dpmm;
    case Unit::Millimeter:
      return size * dpmm;
  }
}

void FontFace::Render(std::vector<vertex>& vertices, const std::string& text, float left, float top, float width, float z, Align align) const {
  (void)align;
  float desiredSizePx = size_px();
  auto metrics = font->GetMetrics();

  float lineHeight = metrics.ascender - metrics.descender + metrics.lineGap;
  float fontHeight = metrics.ascender - metrics.descender;
  float relativeSize = desiredSizePx / fontHeight;
  float currentX = left;
  float currentY = top + desiredSizePx;
  printf("%f %f %f ==> %f %f %f\n", metrics.ascender, metrics.descender, metrics.lineGap, lineHeight, fontHeight, relativeSize);

  for (size_t n = 0; n < text.size(); n++)
  {
    Stile::Mesh m = font->GetGlyph(text[n]);

//    Mesh m = glyph.ToOutline(*font).ToMesh();

    for (auto& face : m.faces) {
      vertices.push_back({currentX + relativeSize * m.vertices[face.v1].x, currentY - relativeSize * m.vertices[face.v1].y, z, 0, 0, 1, 1, 1});
      vertices.push_back({currentX + relativeSize * m.vertices[face.v2].x, currentY - relativeSize * m.vertices[face.v2].y, z, 0, 0, 1, 1, 1});
      vertices.push_back({currentX + relativeSize * m.vertices[face.v3].x, currentY - relativeSize * m.vertices[face.v3].y, z, 0, 0, 1, 1, 1});
    }

    currentX += font->GetGlyphAdvanceWidth(text[n]) * relativeSize;
    if (currentX > left + width) {
      currentX = left;
      currentY += lineHeight * relativeSize;
    }
  }
}


